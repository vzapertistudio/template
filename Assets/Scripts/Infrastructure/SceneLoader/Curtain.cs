﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Curtain : MonoBehaviour
{
    [SerializeField]
    private Slider progressSlider;

    public void SetProgressValue(float progress)
    {
        progressSlider.value = progress;
    }
}
