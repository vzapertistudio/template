﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SceneLoader
{
    private ICoroutineRunner coroutineRunner;
    private ICurtainSpawner curtainSpawner;

    public SceneLoader(ICoroutineRunner coroutineRunner, ICurtainSpawner curtainSpawner)
    {
        this.coroutineRunner = coroutineRunner;
        this.curtainSpawner = curtainSpawner;
    }

    public void Load(string name, Action callBack = null)
        => coroutineRunner.StartCoroutine(LoadScene(name,callBack));

    private IEnumerator LoadScene(string name,Action callBack=null )
    {
        //if (SceneManager.GetActiveScene().name!=name) //if requare
        //{
            GameObject curtain = Resources.Load<GameObject>(Constants.CurtainPath);
            GameObject curtainObj=curtainSpawner.SpawnCurtain(curtain);
            AsyncOperation loadSceneOperation = SceneManager.LoadSceneAsync(name);
            while (!loadSceneOperation.isDone)
            {
                curtainObj.GetComponent<Curtain>().SetProgressValue(loadSceneOperation.progress);
                yield return null;
            }
            callBack?.Invoke();
       // }
    }
}
