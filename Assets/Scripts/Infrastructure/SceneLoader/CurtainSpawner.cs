﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurtainSpawner : MonoBehaviour,ICurtainSpawner,ICoroutineRunner
{
    public GameObject SpawnCurtain(GameObject curtain)
    {
        GameObject spawnedCurtaint = Instantiate(curtain) as GameObject;
        return spawnedCurtaint;
    }
}
