﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EntryPoint : Singleton<EntryPoint>
{
    private GameStateMachine gameStateMachine;

    [Inject]
    private void Construct(GameStateMachine gameStateMachine)
    {
        this.gameStateMachine = gameStateMachine;
    }

    protected override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 100;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        SRDebug.Instance.AddOptionContainer(Cheats.Instance);
        gameStateMachine.Enter<StartState>();
    }
}
