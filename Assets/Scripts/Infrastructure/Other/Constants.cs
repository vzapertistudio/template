﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public static string CurtainPath = "Curtain/Curtain";
    public static string BootSceneName = "Boot";
    public static string GameSceneName = "Game";
    public static int StartRandomLevelNumber = 0;
    public static int NextSceneAfterInit = 0;
}
