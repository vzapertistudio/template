﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SceneSender : MonoBehaviour
{
    private GameStateMachine gameStateMachine;
    private EventService eventService;

    private void OnEnable()
    {
        eventService.PlayerInputEvents.OnNextLevelButtonClick += GoToSceneLoadState;
        eventService.PlayerInputEvents.OnRestartLevelButtonClick += GoToSceneLoadState;
    }

    private void OnDisable()
    {
        eventService.PlayerInputEvents.OnNextLevelButtonClick -= GoToSceneLoadState;
        eventService.PlayerInputEvents.OnRestartLevelButtonClick -= GoToSceneLoadState;
    }

    [Inject]
    private void Construct(GameStateMachine gameStateMachine, EventService eventService)
    {
        this.gameStateMachine = gameStateMachine;
        this.eventService = eventService;
    }

    private void Start()
    {
    }

    private void GoToSceneLoadState( ) //Call from event
    {
        gameStateMachine.Enter<SceneLoadState>();
    }
}
