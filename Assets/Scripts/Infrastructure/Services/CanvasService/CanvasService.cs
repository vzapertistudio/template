﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasService : MonoBehaviour
{
    [SerializeField]
    private StartCanvas startCanvas;

    public StartCanvas StartCanvas => startCanvas;

    [SerializeField]
    private GameLoopCanvas gameLoopCanvas;

    public GameLoopCanvas GameLoopCanvas => gameLoopCanvas;

    [SerializeField]
    private LoseCanvas loseCanvas;

    public LoseCanvas LoseCanvas => loseCanvas;

    [SerializeField]
    private FinishCanvas finishCanvas;

    public FinishCanvas FinishCanvas => finishCanvas;
}
