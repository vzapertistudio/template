﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FinishCanvas : UserCanvas
{
    private EventService eventService;

    [Inject]
    private void Construct(EventService eventService)
    {
        this.eventService = eventService;
    }

    public void OnNextLevelBtnClick()
        => eventService.PlayerInputEvents.CallOnNextLevelButtonClick();
}
