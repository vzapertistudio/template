﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class StartCanvas : UserCanvas
{
    [SerializeField]
    private Text levelText;

    private EventService eventService;
    private LevelNumberPresenter levelNumberPresenter;

    [Inject]
    private void Construct(EventService eventService, LevelNumberPresenter levelNumberPresenter)
    {
        this.eventService = eventService;
        this.levelNumberPresenter = levelNumberPresenter;
    }

    private void Start()
    {
        SetLevelNumber();
    }

    private void SetLevelNumber()
    {
        levelText.text = levelNumberPresenter.PickLevelNumber().ToString();
    }
}
