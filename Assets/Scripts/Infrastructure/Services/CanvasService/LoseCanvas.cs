﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LoseCanvas : UserCanvas
{
    private EventService eventService;

    [Inject]
    private void Construct(EventService eventService)
    {
        this.eventService = eventService;
    }

    public void OnRestartBtnClick()
        => eventService.PlayerInputEvents.CallOnRestartLevelButtonClick();
}
