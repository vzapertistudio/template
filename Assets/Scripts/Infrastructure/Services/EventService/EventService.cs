﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventService 
{
    private SceneEvents sceneEvents = new SceneEvents();

    public SceneEvents SceneEvents => sceneEvents;

    private PlayerInputEvents playerInputEvents = new PlayerInputEvents();

    public PlayerInputEvents PlayerInputEvents => playerInputEvents;
}
