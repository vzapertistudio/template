﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputEvents 
{
    public Action OnRestartLevelButtonClick;

    public void CallOnRestartLevelButtonClick()
        => OnRestartLevelButtonClick?.Invoke();

    public Action OnNextLevelButtonClick;

    public void CallOnNextLevelButtonClick()
        => OnNextLevelButtonClick?.Invoke();
}
