﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSaveData 
{
    private string isLevelsEndKey = "IsLevelsEndKey";
    private string levelPrefabIndex = "LevelprefabIndex";
    private string presenterLevelOffset = "PresenterLevelOffset";
    private string lastRandomScenePrefabIndex = "LastRandomScenePrefabIndex";

    public void SaveIsLevelEndsCondition(bool isLevelsEnd)
    {
        int val = isLevelsEnd ? 1 : 0;
        PlayerPrefs.SetInt(isLevelsEndKey, val);
    }

    public bool LoadIsLevelEndsCondition()
    {
        int val = PlayerPrefs.GetInt(isLevelsEndKey);
        return val == 1 ? true : false;
    }

    public void SaveLevelPrefabIndex(int index)
        => PlayerPrefs.SetInt(levelPrefabIndex, index);

    public int LoadLevelPrefabIndex()
        => PlayerPrefs.GetInt(levelPrefabIndex);

    public void SavePresenterLevelIndexOffset(int offset)
       => PlayerPrefs.SetInt(presenterLevelOffset, offset);

    public int LoadPresenterLevelIndexOffset()
        => PlayerPrefs.GetInt(presenterLevelOffset);

    public void SaveLastRandomScenePrefab(int index)
     => PlayerPrefs.SetInt(lastRandomScenePrefabIndex, index);

    public int LoadLastRandomScenePrefab()
     => PlayerPrefs.GetInt(lastRandomScenePrefabIndex);
}
