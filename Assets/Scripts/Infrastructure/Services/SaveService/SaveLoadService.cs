﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadService 
{
    private LevelSaveData levelSaveData = new LevelSaveData();

    public LevelSaveData LevelSaveData => levelSaveData;
}
