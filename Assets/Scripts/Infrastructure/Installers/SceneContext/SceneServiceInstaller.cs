﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SceneServiceInstaller : MonoInstaller
{
    [SerializeField]
    private CanvasService canvasService;

    public override void InstallBindings()
    {
        Container.Bind<CanvasService>().FromInstance(canvasService).AsSingle();
    }
}
