﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ProjectServiceInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<EventService>().AsSingle();
        Container.Bind<SaveLoadService>().AsSingle();
    }
}
