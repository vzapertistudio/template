﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameStateMachineInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<GameStateMachine>().AsSingle().NonLazy();

        Container.Bind<IGameState>().To<StartState>().AsSingle();
        Container.Bind<IGameState>().To<SceneLoadState>().AsSingle();
        Container.Bind<IGameState>().To<GameLoopState>().AsSingle();

    }
}
