﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SceneWorkInstaller : MonoInstaller
{
    [SerializeField]
    private CurtainSpawner curtainSpawner;

    [SerializeField]
    private ScenePrefabsConfig scenePrefabsConfig;

    [SerializeField]
    private LevelNumberPresenter levelNumberPresenter;

    public override void InstallBindings()
    {
        Container.Bind<SceneLoader>().AsSingle();
        Container.Bind<ICoroutineRunner>().FromInstance(curtainSpawner).AsSingle();
        Container.Bind<ICurtainSpawner>().FromInstance(curtainSpawner).AsSingle();
        Container.Bind<ScenePrefabsConfig>().FromInstance(scenePrefabsConfig).AsSingle();
        Container.Bind<LevelNumberPresenter>().FromInstance(levelNumberPresenter).AsSingle() ;
        Container.Bind<DefaultScenePrefabLoadPicker>().AsSingle();
    }
}
