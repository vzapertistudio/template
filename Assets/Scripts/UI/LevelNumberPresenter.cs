﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelNumberPresenter : MonoBehaviour
{
    private DefaultScenePrefabLoadPicker defaultScenePrefabLoadPicker;
    private ScenePrefabsConfig scenePrefabConfig;
    private EventService eventService;
    private SaveLoadService saveLoadService;
    private int afterLevelsCountEndOffset = 0;

    [Inject]
    private void Construct(DefaultScenePrefabLoadPicker defaultScenePrefabLoadPicker, ScenePrefabsConfig scenePrefabConfig,EventService eventService,SaveLoadService saveLoadService)
    {
        this.defaultScenePrefabLoadPicker = defaultScenePrefabLoadPicker;
        this.scenePrefabConfig = scenePrefabConfig;
        this.eventService = eventService;
        this.saveLoadService = saveLoadService;

        afterLevelsCountEndOffset = saveLoadService.LevelSaveData.LoadPresenterLevelIndexOffset();
    }

    private void OnEnable()
    {
        eventService.PlayerInputEvents.OnNextLevelButtonClick += TryToIncreasseOffset;
    }

    private void OnDisable()
    {
        eventService.PlayerInputEvents.OnNextLevelButtonClick -= TryToIncreasseOffset;
    }


    private void TryToIncreasseOffset()
    {
        if (defaultScenePrefabLoadPicker.IsLevelsCountEnd)
        {
            afterLevelsCountEndOffset++;
            saveLoadService.LevelSaveData.SavePresenterLevelIndexOffset(afterLevelsCountEndOffset);
        }
    }

    public int PickLevelNumber()
    {
        int number;

        if (Cheats.Instance.IsRequareForceEnter)
        {
            number = Cheats.Instance.LoadSceneNumber;
            afterLevelsCountEndOffset = 0;
            saveLoadService.LevelSaveData.SavePresenterLevelIndexOffset(afterLevelsCountEndOffset);
        }
        else
        {
            if (defaultScenePrefabLoadPicker.IsLevelsCountEnd)
            {
                number = scenePrefabConfig.SceneDatasCount + afterLevelsCountEndOffset;
            }
            else
            {
                number = defaultScenePrefabLoadPicker.CurrentScenePrefabNumber;
            }
        }

        return number;
    }
}
