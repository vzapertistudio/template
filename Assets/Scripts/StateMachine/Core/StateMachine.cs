using System;
using System.Collections.Generic;
using System.Linq;

 public abstract class StateMachine<T> where T:IState
 {
    public IState ActiveState => activeState;

    protected  Dictionary<Type, T> states;
    protected IState activeState;

    public StateMachine(List<T> inputStates) 
        => states = inputStates.ToDictionary(state => state.GetType(), state => state);

    public void Enter<TState>() where TState : IState
     {
       activeState?.Exit();
       IState state = states[typeof(TState)];
       activeState = state;
       state.Enter();
     }
 }
