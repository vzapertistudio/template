﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class StartState : IGameState
{
    private LazyInject<GameStateMachine> statemachine;

    public StartState(LazyInject<GameStateMachine>statemachine)
    {
        this.statemachine = statemachine;
    }

    public void Enter()
    {
        Initialize();
        statemachine.Value.Enter<SceneLoadState>();

    }

    public void Exit()
    {

    }

    private void Initialize()
    {
        //Some inits
    }
}
