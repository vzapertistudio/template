﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameLoopState : IGameState
{
    private LazyInject<GameStateMachine> statemachine;

    public GameLoopState(LazyInject<GameStateMachine> statemachine)
    {
        this.statemachine = statemachine;
    }

    public void Enter()
    {
  
    }

    public void Exit()
    {

    }

    private void InitializeLevelPrefab()
    {

    }
}
