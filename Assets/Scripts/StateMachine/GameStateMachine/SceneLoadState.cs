﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SceneLoadState : IGameState
{
    private LazyInject<GameStateMachine> statemachine;
    private DefaultScenePrefabLoadPicker scenePrefabLoadPicker;
    private SceneLoader sceneLoader;
    private ScenePrefabsConfig scenePrefabsConfig;
    private DiContainer diContainer;
    private GameObject spawnedScenePrefab;
    private EventService eventService;
    private SaveLoadService saveLoadService;

    public SceneLoadState(LazyInject<GameStateMachine> statemachine,SceneLoader sceneLoader,ScenePrefabsConfig scenePrefabsConfig, DiContainer diContainer, DefaultScenePrefabLoadPicker scenePrefabLoadPicker,EventService eventService,SaveLoadService saveLoadService)
    {
        this.statemachine = statemachine;
        this.sceneLoader = sceneLoader;
        this.scenePrefabsConfig = scenePrefabsConfig;
        this.diContainer = diContainer;
        this.scenePrefabLoadPicker = scenePrefabLoadPicker;
        this.eventService = eventService;
        this.saveLoadService = saveLoadService;
    }

    public void Enter()
    {
        ClearOldData();
        sceneLoader.Load(Constants.GameSceneName, OnLoadSceneCallBack);
    }

    public void Exit()
    {
    }

    private void OnLoadSceneCallBack()
    {
        InitializeSceneLevelPrefab();
        EnterInGameLoop();
    }

    private void EnterInGameLoop()
       =>statemachine.Value.Enter<GameLoopState>();

    private bool isFirstLevelsEndCondition = true;

    private void InitializeSceneLevelPrefab()
    {
        GameObject scenePrefab;
        int sceneIndex;


        if (Cheats.Instance.IsRequareForceEnter)
        {
            sceneIndex = Cheats.Instance.LoadSceneNumber - 1;
            scenePrefabLoadPicker.SetScenePrefabIndexToZero();
        }
        else
        {
            if (!scenePrefabLoadPicker.IsLevelsCountEnd)
            {
                sceneIndex = scenePrefabLoadPicker.CurrentScenePrefabIndex;
            }
            else
            {
                if (isFirstLevelsEndCondition)
                {
                    sceneIndex = saveLoadService.LevelSaveData.LoadLastRandomScenePrefab();
                    isFirstLevelsEndCondition = false;
                }
                else
                {
                    sceneIndex = Random.Range(Constants.StartRandomLevelNumber, scenePrefabsConfig.SceneDatasCount);
                }
                saveLoadService.LevelSaveData.SaveLastRandomScenePrefab(sceneIndex);
            }
        }
        scenePrefab = scenePrefabsConfig.GetScenesPrefab(sceneIndex).ScenePrefabObj;
        spawnedScenePrefab = diContainer.InstantiatePrefab(scenePrefab);
    }

    private void ClearOldData()
    {
        if (spawnedScenePrefab != null)
            GameObject.Destroy(spawnedScenePrefab);
    }
    
}
