using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CheatsProvider : MonoBehaviour
{
    public static EventService EventService => eventServiceInst;

    private static EventService eventServiceInst;

    [Inject]
    private void Construct(EventService eventService)
    {
        eventServiceInst = eventService;
    }
}
