using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.ComponentModel;

public  class Cheats 
{
    public static readonly Cheats Instance = new Cheats();

    [Category("Scenes")]
    public void RestartLevel()
       => CheatsProvider.EventService.PlayerInputEvents.CallOnRestartLevelButtonClick();

    [Category("Scenes")]
    public bool IsRequareForceEnter
    {
        get { return isRequareForceEnter; }
        set
        {
            isRequareForceEnter = value;
        }
    }

    private bool isRequareForceEnter = false;

    [Category("Scenes")]
    public int LoadSceneNumber
    {
        get { return loadSceneNumber; }
        set
        {
            loadSceneNumber = value;
        }
    }

    private int loadSceneNumber = 1;
}
