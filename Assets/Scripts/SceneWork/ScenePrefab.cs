using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EndGameType {Default,Test }

public class ScenePrefab : MonoBehaviour
{
    public GameObject ScenePrefabObj => gameObject;

    [SerializeField]
    private Transform endGameTransform;

    public Transform EndGameTransform => endGameTransform;
}
