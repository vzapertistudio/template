﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultScenePrefabLoadPicker 
{
    public int CurrentScenePrefabNumber=> currentScenePrefabIndex + 1;

    public int CurrentScenePrefabIndex => currentScenePrefabIndex;

    public bool IsLevelsCountEnd => isLevelsCountEnd;

    private  int currentScenePrefabIndex=0;

    private bool isLevelsCountEnd = false;

    private ScenePrefabsConfig scenePrefabsConfig;

    private EventService eventService;

    private SaveLoadService saveLoadService;

    public DefaultScenePrefabLoadPicker(ScenePrefabsConfig scenePrefabsConfig, EventService eventService, SaveLoadService saveLoadService)
    {
        this.scenePrefabsConfig = scenePrefabsConfig;
        this.eventService = eventService;
        this.saveLoadService = saveLoadService;

        isLevelsCountEnd = saveLoadService.LevelSaveData.LoadIsLevelEndsCondition();
        currentScenePrefabIndex= saveLoadService.LevelSaveData.LoadLevelPrefabIndex();

        eventService.PlayerInputEvents.OnNextLevelButtonClick += TryIncreaseIndex;
    }

    private void TryIncreaseIndex()
    {
        if (scenePrefabsConfig.SceneDatasCount > CurrentScenePrefabNumber)
        {
            currentScenePrefabIndex++;
            saveLoadService.LevelSaveData.SaveLevelPrefabIndex(currentScenePrefabIndex);
        }
        else
        {
            isLevelsCountEnd = true;
            saveLoadService.LevelSaveData.SaveIsLevelEndsCondition(isLevelsCountEnd);
        }
    }

    public void SetScenePrefabIndexToZero()
    {
        currentScenePrefabIndex = 0;
        isLevelsCountEnd = false;
        saveLoadService.LevelSaveData.SaveIsLevelEndsCondition(isLevelsCountEnd);
        saveLoadService.LevelSaveData.SaveLevelPrefabIndex(currentScenePrefabIndex);
    }
}
