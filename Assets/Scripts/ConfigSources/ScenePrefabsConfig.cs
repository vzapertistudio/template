using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Scenes Config", menuName = "SceneConfig", order = 51)]
public  class ScenePrefabsConfig : ScriptableObject
{
    [SerializeField]
    private List<SceneData> sceneDatasDefault;

    public int SceneDatasCount => sceneDatasDefault.Count;

    public ScenePrefab GetScenesPrefab(int forLvl)
    {
        if (sceneDatasDefault[forLvl] != null)
        {
            return sceneDatasDefault[forLvl].ScenePrefab;
        }
        else
        {
            Debug.LogError("Scenes is not initialized or index out of range!");
            return null;
        }
    }

    public EndGameType GetScenesEndGameType(int forLvl)
    {
        if (sceneDatasDefault[forLvl] != null)
        {
            return sceneDatasDefault[forLvl].EngGameType;
        }
        else
        {
            Debug.LogError("Scenes is not initialized or index out of range!");
            return EndGameType.Default;
        }
    }
}


[System.Serializable]
public class SceneData
{
    [SerializeField]
    private ScenePrefab scenePrefabs;

    [SerializeField]
    private EndGameType engGameType;

    public ScenePrefab ScenePrefab => scenePrefabs;

    public EndGameType EngGameType => engGameType;
}
